const cleanDB = require('./helpers/db_cleaner')
const User = require('../models/User')
const Authorize = require('../models/Authorize')
const AuthorizationError = require('../errors/AuthorizationError')
const chai = require('chai')
const chaiAsPromised = require('chai-as-promised')
chai.use(chaiAsPromised)
const expect = chai.expect

const fixtures = require('./fixtures/fixtures')
const createBasicCollection = require('./helpers/basic_collection')

const adminFixture = fixtures.adminUser
const userFixture = fixtures.user

describe('Authorize', () => {
  var collection

  beforeEach(() => {
    return cleanDB().then(
      createBasicCollection
    ).then(
      userAndCol => { collection = userAndCol.collection }
    )
  })

  describe('admin user', () => {
    it('can create fragments', () => {
      var user = new User(adminFixture)
      return user.save().then(
        user => {
          const url = `/api/collections/${collection.id}/fragments`
          return Authorize.can(user.id, 'create', url)
        }
      ).then(
        permission => expect(permission).to.equal(true)
      )
    })
  })

  describe('user without a team', () => {
    it('can not create a fragment', () => {
      var user = new User(userFixture)
      return user.save().then(
        user => {
          const url = `/api/collections/${collection.id}/fragments`

          expect(Authorize.can(
            user.id, 'create', url
          )).to.be.rejectedWith(AuthorizationError)
        }
      )
    })
  })
})
