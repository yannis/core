# Build status

[![build status](https://gitlab.coko.foundation/ci/projects/1/status.png?ref=master)](https://gitlab.coko.foundation/ci/projects/1?ref=master)

# Description

This is the PubSweet Core, to be used as a dependency in PubSweet apps, such as: https://gitlab.coko.foundation/pubsweet/science-blogger

# Rough roadmap/priorities (4th of July, 2016)

- Roles and permissions (https://gitlab.coko.foundation/pubsweet/core/issues/19)
- Collaboration/realtime sync of component state
- Backend extensions

- Backlog in our [kanban board](http://wekan.coko.foundation/b/fawY3QiLDhmY4Z9pf/pubsweet-core)
